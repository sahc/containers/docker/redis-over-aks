## Prerequisito

Cluster Kubernete. Revisar cómo montar un [Cluster AKS con Terraform](https://gitlab.com/sahc/iac/terraform/azure-aks)

Para trabajar remotamente sobre el cluster debemos utilizar el cliente de azure y setear el contexto del Cluster creado con Terraform

En la raíz del proyecto ejecutar "az aks get-credentials --resource-group [nombre_grupo] --name [nombre_aks]"
``` bash
az aks get-credentials --resource-group POC-AKS-Terraform --name axityaks
```

Si se presentan problemas para trabajar con el contexto revisar [aqui](https://stackoverflow.com/questions/37016546/how-do-i-delete-clusters-and-contexts-from-kubectl-config)

# Redis cluster
Un [redis cluster](https://redis.io/topics/cluster-tutorial) corriendo en Kubernetes.

Si la configuración del clúster de un nodo redis se pierde de alguna manera, volverá con una ID diferente, lo que altera el equilibrio en el clúster. Para evitar esto, la configuración utiliza una combinación de Kubernetes StatefulSets y PersistentVolumeClaims para asegurarse de que el estado del clúster se mantenga después de la reprogramación o fallas.

## Setup
``` bash
kubectl apply -f redis-cluster.yml
```
Esto levantará 6 pods `redis-cluster` uno por uno, lo que puede tomar un tiempo.

## Revisando los PODs
AKS habilita una interfaz web para poder revisar el estado del cluster, "az aks browse --resource-group [nombre_grupo] --name [nombre_aks]"
``` bash
az aks browse --resource-group POC-AKS-Terraform --name axityaks
```

## Inicialización
Una vez que todos los pods estén en estado de ejecución, se puede habilitar el clúster utilizando el `redis-cli` en cualquiera de los pods. Después de la inicialización, terminará con 3 nodos maestros y 3 nodos esclavos.
``` bash
kubectl exec -it redis-cluster-0 -- redis-cli --cluster create --cluster-replicas 1 \
$(kubectl get pods -l app=redis-cluster -o jsonpath='{range.items[*]}{.status.podIP}:6379')
```

TODO: Revisar, al momento de realizar la prueba se utilizo un Win10. Obs, el comando entre $() devuelve una lista de IP's separadas por ', que debe ser convertida a lista separada por espacios.

## Agregando nodos
Agregar nodos al clúster implica algunos pasos manuales. Primero, agreguemos dos nodos:
``` bash
kubectl scale statefulset redis-cluster --replicas=8
```

Para que el primer nodo nuevo se una al clúster como maestro:
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster add-node \
$(kubectl get pod redis-cluster-6 -o jsonpath='{.status.podIP}'):6379 \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379
```

El segundo nodo nuevo debe unirse al clúster como esclavo. Esto unirá automáticamente al maestro con el menor número de esclavos (en este caso, `redis-cluster-6`)
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster add-node --cluster-slave \
$(kubectl get pod redis-cluster-7 -o jsonpath='{.status.podIP}'):6379 \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379
```

Finalmente, reequilibramos automáticamente los maestros:
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster rebalance --cluster-use-empty-masters \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379
```

## Eliminando nodos

### Eliminando esclavos
Los esclavos se pueden eliminar de forma segura. Primero, obtengamos la identificación del esclavo:

``` bash
$ kubectl exec redis-cluster-7 -- redis-cli cluster nodes | grep myself
3f7cbc0a7e0720e37fcb63a81dc6e2bf738c3acf 172.17.0.11:6379 myself,slave 32f250e02451352e561919674b8b705aef4dbdc6 0 0 0 connected
```

Después lo eliminamos:
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster del-node \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379 \
3f7cbc0a7e0720e37fcb63a81dc6e2bf738c3acf
```

### Eliminando un maestro
Para eliminar los nodos maestros del clúster, primero debemos mover los slots utilizados por ellos al resto del clúster para evitar la pérdida de datos.

Primero, tome nota de la identificación del nodo maestro que estamos eliminando:
``` bash
$ kubectl exec redis-cluster-6 -- redis-cli cluster nodes | grep myself
27259a4ae75c616bbde2f8e8c6dfab2c173f2a1d 172.17.0.10:6379 myself,master - 0 0 9 connected 0-1364 5461-6826 10923-12287
```

También anote la identificación de cualquier otro nodo maestro:
``` bash
$ kubectl exec redis-cluster-6 -- redis-cli cluster nodes | grep master | grep -v myself
32f250e02451352e561919674b8b705aef4dbdc6 172.17.0.4:6379 master - 0 1495120400893 2 connected 6827-10922
2a42aec405aca15ec94a2470eadf1fbdd18e56c9 172.17.0.6:6379 master - 0 1495120398342 8 connected 12288-16383
0990136c9a9d2e48ac7b36cfadcd9dbe657b2a72 172.17.0.2:6379 master - 0 1495120401395 1 connected 1365-5460
```

Luego, use el comando `reshard` para mover todos los slots de` redis-cluster-6`:
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster reshard --cluster-yes \
--cluster-from 27259a4ae75c616bbde2f8e8c6dfab2c173f2a1d \
--cluster-to 32f250e02451352e561919674b8b705aef4dbdc6 \
--cluster-slots 16384 \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379
```

Después del `reshard`, es seguro eliminar el nodo maestro `redis-cluster-6`:
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster del-node \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379 \
27259a4ae75c616bbde2f8e8c6dfab2c173f2a1d
```

Finalmente, podemos reequilibrar los maestros restantes para distribuir uniformemente los slots:
``` bash
kubectl exec redis-cluster-0 -- redis-cli --cluster rebalance --cluster-use-empty-masters \
$(kubectl get pod redis-cluster-0 -o jsonpath='{.status.podIP}'):6379
```

### Reduciendo la escala
Después de que el maestro haya sido reenviado y ambos nodos hayan sido eliminados del clúster, es seguro reducir la escala del estado:
``` bash
kubectl scale statefulset redis-cluster --replicas=6
```

## Limpieza
``` bash
kubectl delete statefulset,svc,configmap,pvc -l app=redis-cluster
```